/* Nama : angga yudha isgantara
   kelas : IF-1B 
   NIM : 301230046
*/
   #include<iosteram>
   #include<iommanip>
   #include<stdlib.h>

   using namespace std;

   int main()
   {
       system("clear");

       int N = 0;
       float Data = 0.0,Rata = 0.0, Total = 0.0;
       
       int i = 1;
       cout << "masukan berapa data yang di inginkan : ";
       cin >> N;
       Total = 0;
       do
       {
           cout << "masukan Data ke " << i << " : ";
           i++;
           cin >> Data;
           Total += Data;

       }while (i <= N);

       Rata = Total / N;
       cout << "Banyaknya Data : " << N << endl;
       cout << " Total Nilai Data : " << setprecision(2) << Total << endl;
       cout << "Rata-rata Nilai Data : " << setprecision(2) << Rata << endl;

       return 0;

   }
       
    
   
