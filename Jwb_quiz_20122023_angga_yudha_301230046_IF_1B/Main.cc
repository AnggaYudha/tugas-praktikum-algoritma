/*  Nama    :Angga Yudha Isgantara
    Kelas   :IF-1B
    NIM     :301230046
*/

#include <iostream>

using namespace std;

#include "aplikasi.cpp" 

int main()
{
    system("clear");

    string nama = " ";
    char gol = ' ';
    string status = " ";

    float gapok = 0.0;
    float gaber = 0.0;
    float tunja = 0.0;
    float pot = 0.0;

    jdl_aplikasi();

    input(nama, gol, status);
    gapok_tunja(gol, status, gapok, tunja);

    float prosen_pot = prosen_potongan(gapok);
    pot = potongan(gapok, tunja, prosen_pot);
    gaber = gaji_bersih(gapok, tunja, pot);

    output(gapok, tunja, pot, gaber);

    return 0;
}
